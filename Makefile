# Makefile
stage := $(or $(YAAWS_STAGE), dev)

include Makefile.$(stage)

up:
	docker-compose up -d

down:
	docker-compose down --remove-orphans -v

app:
	docker exec -it yaaws_app sh

go-test-run:
	docker exec yaaws_app go run main.go

postgres-test-run:
	docker exec yaaws_postgres psql -U yaroslav -d yaaws -c "SELECT VERSION();"

init-test: up go-test-run postgres-test-run down

# cgo: exec gcc: exec: "gcc": executable file not found in $PATH
temp-fix-gcc:
	docker exec yaaws_app apk add build-base

goose-install:
	go get -u github.com/pressly/goose/v3/cmd/goose

MIGRATION_NAME=$(or $(MIGRATION), yaaws_init)
migrate-create:
	mkdir -p ./storage/schema
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) create $(MIGRATION_NAME) sql

migrate-up:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) up
migrate-redo:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) redo
migrate-down:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) down
migrate-reset:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) reset
migrate-status:
	goose -dir ./storage/schema -table schema_migrations postgres $(POSTGRES_URI) status

generate-sqlc:
	# go install github.com/sqlc-dev/sqlc/cmd/sqlc@latest
	sqlc generate
	# alternative
	# docker run --rm -v $(shell pwd):/src -w /src kjconroy/sqlc generate
