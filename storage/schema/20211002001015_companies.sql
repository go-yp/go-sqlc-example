-- +goose Up
-- +goose StatementBegin
CREATE TABLE companies
(
    id         SERIAL PRIMARY KEY,
    alias      VARCHAR(255) UNIQUE      NOT NULL,
    name       VARCHAR(255)             NOT NULL,
    created_by INT                      NOT NULL,
    created    TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (created_by)
        REFERENCES users (id)
);

CREATE TABLE vacancies
(
    id         SERIAL PRIMARY KEY,
    company_id INT                      NOT NULL,
    title      VARCHAR(255)             NOT NULL,
    created_by INT                      NOT NULL,
    created    TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (company_id)
        REFERENCES companies (id),
    FOREIGN KEY (created_by)
        REFERENCES users (id)
);

CREATE TABLE reviews
(
    id         SERIAL PRIMARY KEY,
    parent_id  INT                      NULL,
    company_id INT                      NOT NULL,
    content    TEXT                     NOT NULL,
    created_by INT                      NOT NULL,
    created    TIMESTAMP WITH TIME ZONE NOT NULL,
    FOREIGN KEY (parent_id)
        REFERENCES reviews (id),
    FOREIGN KEY (company_id)
        REFERENCES companies (id),
    FOREIGN KEY (created_by)
        REFERENCES users (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS reviews;
DROP TABLE IF EXISTS vacancies;
DROP TABLE IF EXISTS companies;
-- +goose StatementEnd
