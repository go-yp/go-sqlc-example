-- name: UserNew :exec
INSERT INTO users (username, created)
VALUES (@username, @created);

-- name: UserNewAndGet :one
INSERT INTO users (username, created)
VALUES (@username, @created)
RETURNING id, username, created;

-- name: UserGetByID :one
SELECT id, username, created
FROM users
WHERE id = @id;

-- name: UsersCount :one
SELECT COUNT(*)
FROM users;

-- name: Users :many
SELECT *
FROM users
LIMIT @sql_limit OFFSET @sql_offset;
