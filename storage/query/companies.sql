-- name: CompanyNewAndGetID :one
INSERT INTO companies (alias, name, created_by, created)
VALUES (@alias, @name, @created_by, @created)
RETURNING id;
