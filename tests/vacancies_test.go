package tests

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"gitlab.com/go-yp/go-sqlc-example/storage/dbs"

	"github.com/stretchr/testify/require"

	_ "github.com/lib/pq"
)

func TestVacancies(t *testing.T) {
	connection, err := sql.Open("postgres", dataSourceName)
	require.NoError(t, err)
	defer connection.Close()

	require.NoError(t, connection.Ping())

	// clear users
	{
		const (
			// language=PostgreSQL
			query = "TRUNCATE TABLE users RESTART IDENTITY CASCADE"
		)

		_, err := connection.Exec(query)
		require.NoError(t, err)
	}

	// clear companies
	{
		const (
			// language=PostgreSQL
			query = "TRUNCATE TABLE companies RESTART IDENTITY CASCADE"
		)

		_, err := connection.Exec(query)
		require.NoError(t, err)
	}

	// clear vacancies
	{
		const (
			// language=PostgreSQL
			query = "TRUNCATE TABLE vacancies RESTART IDENTITY CASCADE"
		)

		_, err := connection.Exec(query)
		require.NoError(t, err)
	}

	var queries = dbs.New(connection)
	var ctx = context.Background()
	var now = time.Now().UTC().Truncate(time.Second)

	// create user
	{
		err := queries.UserNew(ctx, dbs.UserNewParams{
			Username: "System",
			Created:  now,
		})

		require.NoError(t, err)
	}

	{
		id, err := queries.CompanyNewAndGetID(ctx, dbs.CompanyNewAndGetIDParams{
			Alias:     "yaaws",
			Name:      "YAAWS",
			CreatedBy: 1,
			Created:   now,
		})

		require.NoError(t, err)
		require.Equal(t, int32(1), id)
	}

	{
		err := queries.VacancyNew(ctx, dbs.VacancyNewParams{
			CompanyID: 1,
			Title:     "Senior Rust Developer",
			CreatedBy: 1,
			Created:   now,
		})

		require.NoError(t, err)
	}

	{
		expected := []dbs.VacancyByCompaniesRow{
			{
				VacancyID:    1,
				VacancyTitle: "Senior Rust Developer",
				CompanyID:    1,
				CompanyAlias: "yaaws",
				CompanyName:  "YAAWS",
			},
		}

		actual, err := queries.VacancyByCompanies(ctx, []int32{1})
		require.NoError(t, err)
		require.Equal(t, expected, actual)
	}
}
